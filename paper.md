## Fractal Coin
We propose idea of generalized proof of work algorithm with the use of branch-and-bound framework.

## Intro

Brute-force technique is beneficial whenever the alternative procedures doesn't exist or could not be optimized. 

For example, we want to find the maximum of a function $f: \R \mapsto \R$. 
Then solution is a set $S = \{\ x_i\ |\ x_i,x_j \in \R,\ \forall x_{j\neq i} \ f(x_i) \ge f(x_j) \}$

Therefore, finding such set can be achieved by comparing every evaluation of $f(x_j)$.
Bitcoin proof of work is just one particular instance of finding string with maximal count of consecutive zeros, produced by hash.

Evaluation process of $ f(x)$ is working. 

Validation is giving quantification (rank) of particular work.

The idea of having the act of evaluation and validation of $f(x)$ is called mining.

Finding maximum can be realized as finding minimum of a function $g(x)= -f(x)$  and vice versa. Not only that, but we can introduce family of validation functions that accepts solution as appropriate.

**Examples**:

- Roots of polynomial $p(x)$ as finding minimum of $|p(x) - 0|$

- Inverse matrix $A^{-1}$ of a matrix $A$ as finding minimum of $|Ax - I|$

- Shortest path in graph as minimal sum of distances

- Any other optimization problem

Validation is a function $ v: E \mapsto \{true, false\}$ that decides whether $x_i \in E$ is or isn't solution.

But lets extend that idea little further by allowing more than having just $\{true, false\}$ options.
Mainly because of the fact that there could be more seemingly valid $true$ options and we have to distinguish between them
and frankly, any seemingly equal solutions could yield different consequences by their next consumption.

Therefore, we assume $v: E \mapsto \R^n$ to be more precise.

### Wrapping bitcoin  
Proof of work algorithm can be decoupled into  
- Mining layer
  - choose the best possible solution
- Validation layer
  - choose rank for solution 
- Forking layer  
  - choose one from many chains

# Mining layer

In the most general proof of work, miner's just a fast PRNG generator with feed-back  
system that responds to environment based on matching some conditions.  

This recursive behavior can be represented by  3 - functions.
$$
\text{\small Generator} \\
g: \N \prime \mapsto \N \prime \\
\text{\small mapping is considered to be one way only }
$$
$$
\text{\small Validator} \\
v: \N \prime \mapsto \{0,1\} \\
\text{\small mapping is considered to be one way only }
$$
$$
\text{\small Mining} \\
n(x) = v(x) * x + (1-v(x)) * n(g(x)) \\
\text{\small encapsulates mining process } \\
$$
Function $v(x)$ has to accept at least one input in order for $\sum_{k \in \N \prime}^{} n(k) \gt 0$ . 
$$
\begin{align*}
n(x) &= \begin{cases}
   n \in \N &\exists k \in \N \prime,\ V(k) = 1 \\
   halts &\text{else }
\end{cases}
\end{align*}
$$

$$
\sum_{k \in \N \prime}^{} n(k) \geq |\N \prime|
$$

Example

- $v(x) = \text{ isZero}(x^5 + x - 5)$
- $g(x) = \text{nextRandom}(x)$

Scenario:
- $n(3) = 3 * 0 + 1 * n(7)$
- $n(3) = 3 * 0 + 1 * (7 * 0 + 1 * n(1.29915...))$
- $n(3) = 3 * 0 + 1 * (7 * 0 + 1 * (1.29915 + 0 * n(...))))$
- $n(3) = 3 * 0 + 1 * (7 * 0 + 1.29915...)$
- $n(3) = 3 * 0 + 1 * (1.29915...)$
- $n(3) = 1.29915...$

  But what if miner come up with better algorithm than simply randomly finding solution
  and what happens when two or more solutions are equal?

Ideal proof of work
- Conditions should be simple as possible with respect to generality.   
- Mining layer making useful work as a side effect of proving it.    
- Work should be delegable with and without participation.  
- Proving work is easier in logarithmic time than doing actual work.  

# Validation

Validation of one particular proof
$$
V = \max \text{\small v}\lfloor X \rfloor \\
\text{\small the set of the best valid solutions}  \\
$$

# Forking layer

For every undecidable state $x_i \in V$ fork $f_i$ emerges.  
If $|V|$ is the number of soft forks then $|f_i|$ is the length of $i$-th chain.  

$|f_i| \propto$ probability of being mainchain.    
Decision is simple by the same measurements as with the $V$ but different setup.  

$$
\begin{align*}
V \prime &= \max \text{\small Q} \lfloor F \rfloor \\
Q \prime (x) &= |x| & Q \prime (x)\text{\small\ returns length of } x \\
\end{align*} \\
$$

Every fork $f_i$ lives between $V_{n-1}$ and $V_{n+1}$
