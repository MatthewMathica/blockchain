import CLI from './cli';
import { TinyCoinBuilder } from './src/node/tinycoin/builder';
import {NodeBuilder} from './src/node/builder';

// import {test} from './tests';

const tinycoin = new TinyCoinBuilder();

const commandLine = new CLI(tinycoin);
commandLine.run('Init');

// test();

