import commandLineArgs from 'command-line-args';
import commandLineUsage from 'command-line-usage';
import { cyan } from 'chalk';

import readline from 'readline';

// Commands
import TransactionCommand from './commands/transaction';
import HelpCommand from './commands/help';
import TypeCommand from './commands/type';
import StatusCommand from './commands/status';
import VariableCommand from './commands/variable';
import APICommand from './commands/api';
import ChainInfoCommand from './commands/chain';

import { Command } from './command';
import { OptionDefinition } from 'command-line-usage';
import { INodeBuilder } from '../src/node/builder';

export default class CLI {

    copyrights = '\u00A9 2021';
    app_version = '1.0.1';
    title = `
_______   __________________________
_____  | / /_  __ \\\\__  __ \\\\__  ____\\\/
____   |/ /_  / / /_  / / /_  __/
___  /|  / / /_/ /_  /_/ /_  /___
  /_/ |_/  \\\\____/ /_____/ /_____/ ${this.app_version}
${this.copyrights}
`;

    globalVariables: any = {};
    options: Array<OptionDefinition> = [];
    commands = new Map<string, Command>();

    constructor(nodeBuilder: INodeBuilder) {
        this.addCommand(new TypeCommand(nodeBuilder));
        this.addCommand(new APICommand());
        this.addCommand(new HelpCommand());
        this.addCommand(new TransactionCommand());
        this.addCommand(new StatusCommand());
        this.addCommand(new ChainInfoCommand());
    }

    addCommand(command: Command) {
        this.commands.set(command.getName(), command);
        for (let subcommand of command.getChilds()) {
            this.addCommand(subcommand);
        }

        this.options.push(command.getOptions());
    }

    async run(governmental_occupation?: string) {

        // Render CLI
        this.render([]);
        // Arguments on startup
        let commands = commandLineArgs(this.options);
        if (commands) {
            await this.enter(commands);
        }

        // Live CLI
        let result = null;
        while (true) {
            result = await this.getInput('cli> ');
            if (!result) break;

            try {
                commands = commandLineArgs(this.options, {
                    argv: result.split(' ')
                });
                await this.enter(commands);
            } catch (err) {
                console.log('Invalid command, see -h');
            }
        }

    }

    render(data) {
        console.clear();
        console.log(
            commandLineUsage([
                {
                    raw: true,
                    content: `${cyan(this.title)}`
                },
                ...data
            ]));
    }

    setGlobal(data) {
        const { key, value } = data;
        this.globalVariables[key] = value;
    }

    getGlobal(key) {
        if (!(key in this.globalVariables)) {
            this.setGlobal({ key, value: null });
        }
        return this.globalVariables[key];
    }
    /* - process input commands
       - group results
       - render results
     */
    async enter(commands) {
        let results = [];

        console.log('Processing...');
        for (let command in commands) {
            // Global-variable map
            // If command is not recognized
            // then we assume it is value
            // to be used by existing commands
            if (this.commands.has(command)) {

                const params = {
                    context: this,            // Global parameters
                    local: commands[command] // Parameters for command
                };

                try {
                    // Execute command with parameters
                    const result = await this.commands.get(command).execute(params, this.options);
                    // Add new result to group
                    results = results.concat(result);
                } catch (err) {
                    console.log(err);
                    return {
                        message: 'Error',
                        value: err
                    }
                }
            }
            // Render all results
            this.render(results);
        }
    }

    // Read new line only with user input
    getInput(query): Promise<string> {
        const rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout,
        });

        return new Promise(resolve => rl.question(query, async ans => {
            rl.close();
            resolve(ans);
        }));
    }

}
