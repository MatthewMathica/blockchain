import { Command, CommandResult, Options, Params } from '../command';
import fs from 'fs';

import { INodeBuilder } from '../../src/node/builder';
import { INode } from '../../src/node/node';

import PeerId from 'peer-id';
import VariableCommand from './variable';
import { Modes } from '../../src/node/network';

import {FullNode, LightNode} from '../../src/node/tinycoin/tinycoin';

export default class TypeCommand extends Command {

  private nodeBuilder: INodeBuilder;

  constructor(nodeBuilder: INodeBuilder){

    super({
      name: 'type',
      type: String,
      description: 'Type of operating node',
      typeLabel: '[{underline full}, {underline light}]'
    });

    this.addChild(new VariableCommand({
      name: 'identity',
      type: String,
      description: 'Path to json identity file which holds the asymmetric keys'
    }));

    this.addChild(new VariableCommand({
      name: 'network',
      type: String,
      description: 'Node network network',
      typeLabel: '[{underline desktop}, {underline browser}]'
    }, (network) => {
      switch (network) {
        case "desktop":
          return Modes.Tcp;
          break;
        case "browser":
          return Modes.Browser;
        default:
          return null;
      }
    }));

    this.addChild(new VariableCommand({
      name: 'port',
      type: Number,
      description: 'Node network port'
    }));

    // Assign node assembler
    this.nodeBuilder = nodeBuilder;
  }

  async execute(params: Params, options: Options): Promise<CommandResult> {
    const {context, local} = params;

    let node = context.getGlobal('node');
    // Node is already running
    if (node) {
      return [{
        header: 'Error',
        content: [
          `Cannot initialize node:`,
          `Another node is already running`
        ]
      }];
    }

    let identity: PeerId = null;
    let nodeType: INode = null;

    const type: string = local;
    const identityConfig: string = context.getGlobal('identity');
    const network: string = context.getGlobal('network');
    const port: number = context.getGlobal('port');

    if (type == null) {
      return [{
        header: 'Error',
        content: [
          `Cannot initialize node:`,
          `Node type not specified, use --type`
        ]
      }];
    }

    switch(type) {
      case 'full':
        nodeType = new FullNode();
        break;
      case 'light':
        nodeType = new LightNode();
        break;
      default:
        return [{
          header: 'Error',
          content: [
            `Cannot initialize node:`,
            `Node type '${type}' does not exist, see --help`
          ]
        }];
    }

    if (network == null) {
      return [{
        header: 'Error',
        content: [
          `Cannot initialize node:`,
          `Network not specified, use --network`
        ]
      }];
    }

    if (identityConfig) {
      try {
        const fileContent = fs.readFileSync(identityConfig, 'utf8');
        const config = JSON.parse(fileContent);
        identity = await PeerId.createFromJSON(config);
      } catch (err) {
        return [{
          header: 'Error',
          content: [
            `Couldn't read identity config from ${identityConfig}`
          ]
        }];
      }
    }

    let config = {
      identity, network, port, nodeType
    };

    // Create node
    node = await this.nodeBuilder.build(config);

    // Initialize node
    await node.init();

    // Set node instance to be globally accessible
    context.setGlobal({
      key: 'node',
      value: node
    });

    return [{
      header: 'Info',
      content: [
        `${node._whoami} is running with address ${node.getNetwork().getPublicAddress()}`
      ]
    }];
  }

};
