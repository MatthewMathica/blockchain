import {Command, CommandResult, Options, Params} from '../command';

export default class HelpCommand extends Command {

  constructor(){
    super({
      name: 'help',
      alias: 'h',
      type: Boolean,
      description: 'Show commands'
    });
  }

  async execute(params: Params, options: Options): Promise<CommandResult> {
    return [
      {
        header: 'Options',
        optionList: options
      },
      {
        header: 'Example',
        content: [
          '$ node --transaction "Hello World"',
          '$ node -t "Hello World"'
        ]
      }
    ];

  }
}
