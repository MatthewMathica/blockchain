import {OptionDefinition} from 'command-line-usage';
import {Command, CommandResult, Options, Params} from '../command';

export default class VariableCommand extends Command {

  mapper: (data: any) => any;

  constructor(optionDefinition: OptionDefinition, mapper?: (data: any) => any){
    super(optionDefinition);
    this.mapper = mapper;
  }

  async execute(params: Params, options: Options): Promise<CommandResult> {

    const {context, local} = params;

    context.setGlobal({
      key: this.getName(),
      value: this.mapper ? this.mapper(local) : local
    });

    return [];
  }

}
