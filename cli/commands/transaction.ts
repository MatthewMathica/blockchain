import {Command, CommandResult, Options, Params} from '../command';

import { TransactionController } from '../../api/controllers/transaction';

export default class TransactionCommand extends Command {

  constructor(){
    super({
      name: 'transaction',
      alias: 't',
      type: String,
      description: 'Send transaction',
    });
  }

  async execute(params: Params, options: Options): Promise<CommandResult> {
    const {context, local} = params;

    const node = context.getGlobal('node');

    if (!node) {
      return [{
        header: 'Error',
        content: [
          `Cannot send transaction, no nodes are visible or running`
        ]
      }];
    }

    new TransactionController(node).handle({
      val: local,
      amount: 1
    });

    return [{
      header: 'Info',
      content: [
        `Transaction with value ${local} was succesfully sended`
      ]
    }];
  }
}
