import { UI } from '../../api/rest';
import {Command, CommandResult, Options, Params} from '../command';
import VariableCommand from './variable';

export default class APICommand extends Command {

  constructor(){
    super({
      name: 'api',
      type: Number,
      description: 'Run node api web-server'
    });
  }

  async execute(params: Params, options: Options): Promise<CommandResult> {

    const {context, local} = params;

    const node = context.getGlobal('node');
    if (!node) {
      return [{
        header: 'Error',
        content: [
          `Cannot init API, node is not running`
        ]
      }];
    }

    const api = new UI(node, local);

    return [
      {
        header: 'API',
        content: [
          `Running on *:${api.port}`,
        ]
      }
    ];
  }
}
