import {Command, CommandResult, Options, Params} from '../command';
import {green, cyan} from 'chalk';
import { INetwork } from '../../src/node/network';

export default class StatusCommand extends Command {

  constructor(){
    super({
      name: 'status',
      type: Boolean,
      description: 'Show node status',
    });
  }

  async execute(params: Params, options: Options): Promise<CommandResult> {
    const {context, local} = params;

    const node = context.getGlobal('node');
    if (!node) {
      return [{
        header: 'Error',
        content: [
          `Cannot show status, no nodes are visible or running`
        ]
      }];
    }

    const network: INetwork = node.getNetwork();
    const peers = network.getPeerAddresses();

    return [
      {
        header: 'Status',
        content: [
          `Node is ${green('online')}`,
          `Address: ${network.getPublicAddress()}`,
          `Type: ${cyan(node._whoami)}`
        ]
      },
      {
        header: 'Network',
        content: network.getMultiAddress()
      },
      {
        header: 'Peers',
        content: peers.length > 0 ? peers : ['No peers visible']
      }
    ];
  }
}
