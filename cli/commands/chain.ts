import { Command, CommandResult, Options, Params } from '../command';
import { green, cyan } from 'chalk';
import { INetwork } from '../../src/node/network';
import { INode } from '../../src/node/node';
import { IBlockchain } from '../../src/node/blockchain';

export default class ChainInfoCommand extends Command {

  constructor() {
    super({
      name: 'chain',
      type: Boolean,
      description: 'Show chain status',
    });
  }

  async execute(params: Params, options: Options): Promise<CommandResult> {
    const { context, local } = params;

    const node: INode = context.getGlobal('node') as INode;
    if (!node) {
      return [{
        header: 'Error',
        content: [
          `Cannot show status, no nodes are visible or running`
        ]
      }];
    }

    // TODO: Stream from file descriptor / indexdb
    return [
      {
        header: 'Blockchain',
        content: node.chain.blocks.map((block, index) => `${index}|${block.getHash()}`)
      }
    ];
  }
}

