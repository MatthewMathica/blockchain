import {OptionDefinition} from 'command-line-usage';

type Pair = Record<string, any>;
export type CommandResult = Array<Pair>;
export type Params = any;

export type Options = Array<OptionDefinition>;

export class Command {
  options: OptionDefinition;
  childs: Array<Command> = [];

  constructor(options: OptionDefinition) {
    this.options = options;
  }

  addChild(c: Command) {
    this.childs.push(c);
  }

  getChilds() {
    return this.childs;
  }

  getName(): string {
    return this.options.name;
  }

  getOptions(): OptionDefinition {
    return this.options;
  }

  async execute(params: Params, args: Options): Promise<CommandResult> {
    throw new Error('Not implemented');
  }
};
