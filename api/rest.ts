import WebSocket, { WebSocketServer } from 'ws';
import { TransactionController, TransactionRequest } from './controllers/transaction';

import { IBlock } from '../src/node/block';
import { INode } from '../src/node/node';
import { MessageType } from '../src/io/message';
import { BaseTransaction } from '../src/node/transaction';

export class UI {

  static DEFAULT_PORT: number = 7171;
  public port: number;
  private wss: WebSocketServer;

  private transactionController: TransactionController;

  constructor(node: INode, port: number) {
    const that = this;
    this.port = port || UI.DEFAULT_PORT;
    // Init controllers
    this.transactionController = new TransactionController(node);

    this.wss = new WebSocketServer({ port: this.port });
    this.wss.on('connection', function connection(ws) {
      // Send blockchain for every new visitor
      that.sendEvent(ws, 'node', {
        name: node._whoami,
        friends: node.getNetwork().friends,
        chain: node.chain.blocks
      });

      // Listen for commands
      ws.on('message', function (message) {
        const {eventType, data} = JSON.parse(message);
        if (eventType == "send:transaction") {
          that.transactionController.handle(data);
        }
      });
    });

    // When peer changes status (online, offline, address)
    node.getNetwork().events.on("peer", (peer) => {
      that.boradcastEvent('peer', peer);
    });

    node.events.on(MessageType.TRANSACTION, (t: BaseTransaction) => {
      that.boradcastEvent('transaction', t);
    });

    node.events.on(MessageType.BLOCK, (b: IBlock) => {
      that.boradcastEvent('blockchain', node.chain.blocks.map(b => {
        return {
          prevHash: b.prevHash,
          hash: b.getHash()
        }
      }));
    });
  }

  sendEvent(ws, event, data) {
    const payload = {
      eventType: event,
      data: data
    };
    ws.send(JSON.stringify(payload));
  }

  boradcastEvent(event, data) {
    const payload = {
      eventType: event,
      data
    };

    const serialized = JSON.stringify(payload);
    for (let client of this.wss.clients.values()) {
      if (client.readyState === WebSocket.OPEN) {
        client.send(serialized);
      }
    }
  }
};
