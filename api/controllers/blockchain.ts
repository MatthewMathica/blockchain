import { INode } from '../../src/node/node';
import Controller from '../controller';
import { Transaction } from '../../src/node/tinycoin/transaction';

export type BlockchainRequest = {
    start: number,
    end: number
}

export class BlockchainController extends Controller<BlockchainRequest> {

    constructor(node: INode) {
        super(node);
    }

    handle(data: BlockchainRequest) {

    }
}
