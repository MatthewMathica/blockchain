import { INode } from '../../src/node/node';
import Controller from '../controller';
import { Transaction } from '../../src/node/tinycoin/transaction';

export type TransactionRequest = {
    val: number,
    amount: number
}

export class TransactionController extends Controller<TransactionRequest> {

    constructor(node: INode) {
        super(node);
    }

    handle(data: TransactionRequest) {
        const source = this.node.getNetwork().getPublicAddress();

        for (let i = 0; i < data.amount; i++) {
            let tx = new Transaction();
            tx.source = source;
            tx.val = data.val;

            this.node.sendTransaction(tx);
        }
    }
}
