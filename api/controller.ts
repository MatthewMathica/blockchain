import {INode} from '../src/node/node';

export default abstract class Controller<T> {

  node: INode;

  constructor(node: INode){
    this.node = node;
  }

  abstract handle(data: T): void;
}
