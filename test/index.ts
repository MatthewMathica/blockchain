import { INode } from '../src/node/node';
import { FullNode } from '../src/node/tinycoin/tinycoin';
import { DogeLikeWork } from '../src/node/tinycoin/consensus';
import { TinyCoinBuilder } from '../src/node/tinycoin/builder';

import { Transaction } from '../src/node/tinycoin/transaction';

import { Modes } from '../src/node/network';

import { TXPool, Selector } from '../src/node/txpool';
import { BaseTransaction } from '../src/node/transaction';
import { Block } from '../src/node/tinycoin/block';

// Test Utils
import { generateTransactions } from './utils';
import { testBlockchain } from './testBlockchain';

export async function initNodes(n: number): Promise<INode[]> {

  let nodes: Array<INode> = [];
  for (let i = 0; i < n; i++) {
    new TinyCoinBuilder()
      .build({
        nodeType: new FullNode(),
        network: Modes.Tcp
      })
      .then(s => nodes.push(s));
    console.log(`Composing ${i}-th node`);
  }

  //Start all nodes
  return Promise.all(nodes.map(s => s.init()));
}

async function testTXPool(): Promise<boolean> {

  const pool = new TXPool();

  pool.setMaxSize(10);

  const selector: Selector = (txs: BaseTransaction[]) => {

    const block = new Block();
    const chunk = txs.slice(0, Block.size)

    for (let i = 0; i < Block.size; i++) {
      block.addTransaction(chunk[i]);
    }
    return block;
  };

  pool.addAll(generateTransactions(40));

  const txr = new Transaction();
  txr.val = 1;
  pool.remove([txr]);
  let block = pool.select(selector);

  if (block.merkleRoot != "c09b67e05cfd2dad9faf2409a6d622aa163e7ee8c63c9073e610efde2f1b9390") return false;

  pool.add(txr);
  pool.remove(block.transactions);
  block = pool.select(selector);

  return block.merkleRoot == "b038089f5c3df76a359d33a489c8abaa81311d40c0d946366a564937ff290fc6";
}

export async function test(): Promise<boolean> {

  const isTrue = (val: boolean) => val === true;

  const res = await Promise.all([
    testTXPool(),
    testBlockchain()
  ]);
  const passed = res.every(isTrue);

  if (passed) {
    console.log('Tests passed');
    return true;
  }

  console.log('Tests failed');

  return false;
}
