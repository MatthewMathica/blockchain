import { expect } from 'chai'
import { generateBlocks, getValidBlocks, shuffle } from '../../utils';
import { DogeLikeWork } from '../../../src/node/tinycoin/consensus';
import { Blockchain, MetaBlock } from '../../../src/node/tinycoin/blockchain';
import { Block } from '../../../src/node/tinycoin/block';

describe('Blockchain', async () => {

  it('check deterministic forking', async () => {
    const winner = await getValidBlocks(17);
    const blocks: any[] = [
      winner,
      await getValidBlocks(13),
      await getValidBlocks(7),
      await getValidBlocks(5),
    ];

    shuffle(blocks);

    const observedBlocks = blocks.reduce((arr, c) => [...arr, ...c], []);

    const consensusAt = 5;
    const bc: Blockchain = new Blockchain(consensusAt);
    for (const block of observedBlocks) {
      bc.addBlock(block);
    }

    // expect(bc.blocks.splice(0,m)).to.be.eql(winner.splice(0,m));
    // expect(ghostChain.map(b => b.getHash())).to.be.eql(bc.blocks.map(b => b.getHash()));
  });

  it('merges ghostchain', async () => {
    const ghostChain = await getValidBlocks(5);

    const bc: Blockchain = new Blockchain(4);
    for (const block of ghostChain) {
      bc.addBlock(block);
    }

    expect(bc.blocks.length).equal(ghostChain.length);
    expect(ghostChain.map(b => b.getHash())).to.be.eql(bc.blocks.map(b => b.getHash()));
  });

  it('get path from metablock', async () => {

    const chain = new Blockchain(1);
    const ghostChain = await getValidBlocks(3);
    const metaGhostChain = ghostChain.map(b => new MetaBlock(b));
    for (let i = 1; i < metaGhostChain.length; i++) {
      metaGhostChain[i].prev = metaGhostChain[i - 1];
    }

    const res = chain.getPath(metaGhostChain.pop()).map(b => b.val);
    expect(res).to.be.eql(ghostChain.splice(1));
  });


});
