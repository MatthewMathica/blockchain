import { expect } from 'chai';
import { MerkleTree } from '../../../src/node/merkle';
import { retrieveBlock, retrieveMerkleTree, storeBlock, storeMerkleTree, storeBlocks, retrieveBlocks } from '../../../src/storage';
import { generateBlocks, generateTransactions, getValidBlocks } from '../../utils';

describe('Persistence', () => {

    it('Store then retrieve merkleTree', async () => {
        const txs = generateTransactions(16);
        const mtree = new MerkleTree(txs);
        await storeMerkleTree(mtree);

        const key = mtree.getRoot();
        const value = await retrieveMerkleTree(key);

        expect(value).to.be.eql(mtree.getTree());
    });

    it('Store then retrieve block', async () => {
        const blocks = await getValidBlocks(4)

        await storeBlock(blocks[0]);
        const key = blocks[0].getHash();
        const value = await retrieveBlock(key);
    
        expect(value).to.be.eql(blocks[0]);
    })
    
    it('Store blocks then retrieves it', async () => {
        const blocks = await getValidBlocks(3);
        await storeBlocks(blocks)

        const keys = blocks.map((block) => block.getHash());
        const values = await retrieveBlocks(keys);

        expect(blocks).to.be.eql(values)
    })

});