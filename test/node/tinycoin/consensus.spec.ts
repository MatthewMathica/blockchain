import { expect } from 'chai'
import { TransactionPool } from '../../../src/node/tinycoin/txpool'
import { BaseTransaction } from '../../../src/node/transaction'
import { DogeLikeWork } from '../../../src/node/tinycoin/consensus'

import { Block } from '../../../src/node/tinycoin/block';
import { generateTransactions } from '../../utils';

import { MessageType } from '../../../src/node/tinycoin/message'

describe('DogeWorker', async () => {

  it('mines block', async () => {
    const pool: TransactionPool = new TransactionPool();
    pool.addAll(generateTransactions(10));
    const worker = new DogeLikeWork();
    const block: Block = pool.select();
    const minedBlock: Block = await worker.mine(1, block);
  });

  it('mines whole txpool', async() => {
    const pool: TransactionPool = new TransactionPool();
    pool.addAll(generateTransactions(20));

    const worker = new DogeLikeWork();
    worker.setPool(pool);

    const minedBlocksPromise: Promise<Block[]> = new Promise(res => {
        let blocks: Block[] = [];
        worker.events.on(MessageType.BLOCK, (b: Block) => {
          if (b == null) {
            res(blocks);
          } else blocks.push(b);
        });
    });

    await worker.mineBlocks(1);

    const minedBlocks = await minedBlocksPromise;
    expect(minedBlocks.length).to.be.equal(5);
    expect(pool.isEmpty()).to.be.equal(true);
  })

});
