import { expect } from 'chai';

import { Transaction } from '../../../src/node/tinycoin/transaction';

import { TransactionPool} from '../../../src/node/tinycoin/txpool';
import { BaseTransaction } from '../../../src/node/transaction';

import { IBlock } from '../../../src/node/block'
// Test Utils
import { generateTransactions } from '../../utils';

describe('TransactionPool', () => {

    it('removes txs', () => {

        const txs = generateTransactions(40);
        const pool = new TransactionPool();
        pool.setMaxSize(5);
        pool.addAll(txs);

        pool.remove(txs.slice(0,2));

        pool.add(txs[4]);
        pool.add(txs[3]);

        expect(pool.pool.slice(0,5)).to.satisfy(function (txs) {
            return txs.every(function (tx) {
                return tx.removed == false;
            });
        });
        expect(txs[4]).to.be.equal(pool.pool[0].tx);
        expect(txs[3]).to.be.equal(pool.pool[1].tx);
    });

    it ('selects txs into block', () => {

        const pool = new TransactionPool();

        pool.setMaxSize(10);
        pool.addAll(generateTransactions(40));

        const txr: BaseTransaction = new Transaction();
        txr.val = 1;
        pool.remove([txr]);
        const block: IBlock = pool.select();
        expect(block.merkleRoot).to.be.equal("c09b67e05cfd2dad9faf2409a6d622aa163e7ee8c63c9073e610efde2f1b9390");
        pool.add(txr);

        pool.remove(block.transactions);
        const block2: IBlock = pool.select();

        expect(block2.merkleRoot).to.be.equal("b038089f5c3df76a359d33a489c8abaa81311d40c0d946366a564937ff290fc6");

    })

});
