import { expect } from 'chai';
import { MerkleTree } from '../../../src/node/merkle'
import { Transaction } from '../../../src/node/tinycoin/transaction';
import { generateTransactions } from '../../utils';


describe('Merkle Tree', () => {

  it('verify valid transaction', async () => {
    const txs = generateTransactions(16);
    const tree = new MerkleTree(txs);

    const tx_hash = txs[2].getHash();

    const proof = tree.getProof(tx_hash);

    const isValid = MerkleTree.verify(tree.getRoot(), tx_hash, proof);
    expect(isValid).to.be.equal(true);
  });

  it('verify invalid transaction', async () => {
    const txs = generateTransactions(16);
    const tree = new MerkleTree(txs);

    const tx = new Transaction();
    tx.source = 'bogdanoff';
    tx.target = 'wojak';

    const tx_hash = tx.getHash();

    const proof = tree.getProof(tx_hash);
    const isValid = MerkleTree.verify(tree.getRoot(), tx_hash, proof);
    
    expect(isValid).to.be.equal(false);
  });

});
