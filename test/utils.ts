import { Block, Genesis } from "../src/node/tinycoin/block";
import { Blockchain } from "../src/node/tinycoin/blockchain";
import { Transaction } from "../src/node/tinycoin/transaction";
import { BaseTransaction } from "../src/node/transaction";
import { DogeLikeWork } from '../src/node/tinycoin/consensus';


// INPLACE SHUFFLE
export function shuffle(arr: any[]): void {
  const choice = () => Math.round(Math.random() * (arr.length - 1));
  for (let i = 0; i < arr.length; i++) {
    const choosedIndex = choice();
    const tmp = arr[choosedIndex];
    arr[choosedIndex] = arr[i];
    arr[i] = tmp;
  }
}

export function generateTransactions(n: number): BaseTransaction[] {
  let arr = [];
  for (let i = 0; i < n; i++) {
    const tx = new Transaction();
    tx.val = i;
    arr.push(tx);
  }
  return arr;
}

export function generateBlocks(n: number): Block[] {
  let arr: Block[] = [];
  for (let i = 0; i < n; i++) {
    const block = generateTransactions(Block.size)
      .reduce((b, tx) => {
        b.addTransaction(tx);
        return b;
      } , new Block());
    arr.push(block);
  }

  return arr;
}

export function generateBlockchain(maxDifference: number, length: number): Blockchain {

  const chain = new Blockchain(maxDifference);

  const [genesis, ..._] = chain.blocks;

  // Generate blocks
  let blocks = [genesis, ...generateBlocks(length - 1)];

  // Link blocks
  for (let i = 1; i < blocks.length; i++) {
    blocks[i].prevHash = blocks[i - 1].getHash();
    chain.addBlock(blocks[i]);
  }

  return chain;
}

export async function getValidBlocks(numOfBlocks: number): Promise<Block[]> {

  const miner = new DogeLikeWork();
  const [genesis, ...blocks] = [ Genesis(), ...generateBlocks(numOfBlocks - 1) ];
  let prev = genesis;
  for (const block of blocks) {
    block.prevHash = prev.getHash();
    prev = await miner.mine(1, block);
  }

  return [genesis, ...blocks];
}
