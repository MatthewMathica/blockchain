import { Block } from "../src/node/tinycoin/block";
import { Blockchain, MetaBlock } from "../src/node/tinycoin/blockchain";
import { generateBlockchain } from "./utils";

export async function testBlockchain(): Promise<boolean> {
  return generateBlockchain(3, 10).blocks.length === 10;;
}




