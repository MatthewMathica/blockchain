<p align="center">
   <img src="https://imgur.com/Ujhj9ls.png" alt="Logo">
</p>

<div align="center">
<h3> TSCoin </h3>
    Distributed and parallel computing on blockchain
    <br>
    <a href="https://gitlab.com/MatthewMathica/blockchain/-/issues/new">Report bug</a>
    ·
    <a href="https://gitlab.com/MatthewMathica/blockchain/-/issues/new">Request feature</a>
</div>

## Table of contents

- [Quick start](#quick-start)
- [What's included](#whats-included)
- [Bugs and feature requests](#bugs-and-feature-requests)
- [Contributing](#contributing)
- [Creators](#creators)
- [Copyright and license](#copyright-and-license)


## Quick start
### Install dependencies
```shell
git clone git@gitlab.com:MatthewMathica/blockchain.git
# Blockchain
cd ./blockchain
npm install
# [Optional]
cd ./ui
npm install
```

###  Core development
```shell
# Build
npm run tsc:b
# As desktop
node build/index.js --network desktop --type full
# As browser
node build/index.js --network browser --type light 
```
### UI development
```shell
cd ./ui
npm run dev
# localhost:3000
```

## What's included


### Core package
| Module  | State | Description |
| ------------- | ------------- |  ------------- |
| crypto  | :white_check_mark: | Cryptographic functions
| io  | :white_check_mark:  | Event definitions
| network  | :white_check_mark:  | Network protocols
| storage  |  :white_check_mark:  | Storage socks
| node  | :hammer:   | Blockchain Logic

### UI package
| Module  | State | Description |
| ------------- | ------------- |  ------------- |
| socket-relay  | :hammer:  | Event definitions
| front-end  | :hammer:  | Graph visualization

### Client package
| Module  | State | Description |
| ------------- | ------------- |  ------------- |
| psyllium  | :hammer:  | Chat protocol
| wallet  | :heavy_multiplication_x:  | SVP interface
| transformer  | :heavy_multiplication_x:  | NP contra-reductions

## Bugs and feature requests

Have a bug or a feature request?  [please open a new issue](https://gitlab.com/MatthewMathica/blockchain/-/issues/new).

Security vulnerabilities via private message.

## Contributing
Go hard or go fud.

## Creators

- <https://gitlab.com/MatthewMathica>
- <https://gitlab.com/martin.reguly>


## Copyright and license
[MIT](https://gitlab.com/MatthewMathica/blockchain/-/blob/master/LICENSE) © 2021
