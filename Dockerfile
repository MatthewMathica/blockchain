FROM node:14

WORKDIR /usr/src/node

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run tsc:b