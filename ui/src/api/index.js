const EventEmitter = require('alpeventemitter');

export default function initAPI(port) {
    const socket =  new WebSocket(`ws://localhost:${port}`);
    const events = new EventEmitter();
    socket.addEventListener('open', function (event) {
        console.log('Connected');
    });

    // Listen for messages
    socket.addEventListener('message', function (event) {
        const {eventType, data} = JSON.parse(event.data);
        events.emit(eventType, data);
    });

    events.on(["send:transaction", "send:node"], (data, type) => {

        const payload = {
            eventType: type,
            data
        };

        socket.send(JSON.stringify(payload));
    });

    return events;
};
