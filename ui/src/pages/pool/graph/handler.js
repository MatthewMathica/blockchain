export default class GraphHandler {

    constructor(events, graph) {
        this.graph = graph;
        this.events = events;
    }

    handle(event, fn) {
        const graph = this.graph;
        this.events.on(event, function (data, type) {
            fn(graph, data);
        });
    }
};
