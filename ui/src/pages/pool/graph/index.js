import Dag from './dag';
import GraphHandler from './handler';

export function initDag(element, events) {

    const dag = new Dag(element);
    dag.init();

    const handler = new GraphHandler(events, dag);
    handler.handle("blockchain", (graph, data) => {
        data.blockchain.forEach(function (block) {
            graph.addNode(block);
        });
        graph.render();
    });

    return handler;
}
