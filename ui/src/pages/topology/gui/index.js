import * as dat from 'dat.gui';

export default function initGui(element, events) {
    const gui = new dat.GUI({
        name: 'Menu',
        preset: 'Connect',
        domElement: element
    });

    let node = {
        transaction: {
            val: 12,
            amount: 1,
            send() {
                events.emit("send:transaction", {
                    val: this.val,
                    amount: this.amount
                });
            }
        }
    };

    const fractal = gui.addFolder('Fractal Node');
    const transaction = gui.addFolder('Transaction');

    transaction.add(node.transaction, 'val');
    transaction.add(node.transaction, 'amount', 1, 100).name('copy');
    transaction.add(node.transaction, 'send');

    transaction.open();
}
