import initApi from './../../api';
import {initGraph} from './graph';
import initGui from './gui';

import '../../css/style.css';

// Events from external Node
const events = initApi(7171);

const graph = document.getElementById('graph');

initGraph(graph, events);
initGui(graph, events);

