import ForceGraph from 'force-graph';

export default class Graph {

    constructor(element) {
        this.element = element;
        this.myNode = {
            address: 0,
            color: '#000000'
        };

        this.data = {
            nodes: [],
            links: []
        };

    }

    init() {

        this.addNode(this.myNode);
        const {offsetHeight} = this.element.parentNode;

        this.graph = ForceGraph()(this.element)
            .height(offsetHeight)
            .linkDirectionalParticleSpeed(0.05)
            .linkDirectionalParticleColor(() => 'red')
            .graphData(this.data)
            .nodeCanvasObject((node, ctx, globalScale) => {
                const label = node.id;
                const fontSize = 12/globalScale;
                ctx.font = `${fontSize}px Sans-Serif`;
                const textWidth = ctx.measureText(label).width;
                const bckgDimensions = [textWidth, fontSize].map(n => n + fontSize * 0.2); // some padding

                ctx.fillStyle = 'rgba(0, 0, 0, 1)';
                ctx.fillRect(node.x - bckgDimensions[0] / 2, node.y - bckgDimensions[1] / 2, ...bckgDimensions);

                ctx.textAlign = 'center';
                ctx.textBaseline = 'middle';
                ctx.fillStyle = node.color || 'rgba(255, 255, 255, 1)';
                ctx.fillText(label, node.x, node.y);
                node.__bckgDimensions = bckgDimensions; // to re-use in nodePointerAreaPaint
            })
            .nodePointerAreaPaint((node, color, ctx) => {
                ctx.fillStyle = color;
                const bckgDimensions = node.__bckgDimensions;
                bckgDimensions && ctx.fillRect(node.x - bckgDimensions[0] / 2, node.y - bckgDimensions[1] / 2, ...bckgDimensions);
            });

    }

    clear() {
        this.data = {
            nodes: [],
            links: []
        };
    }

    emitParticles(links) {
        for (let link of links) {
            this.graph.emitParticle(link);
        }
    }

    addNode(node) {
        this.data.nodes.push({
            id: node.address
        });
        if (node.address == this.myNode) return;

        this.data.links.push({
            source: this.myNode.address,
            target: node.address
        });
    }

    removeNode(node) {
        const indexV = this.data.nodes.findIndex(n => n.id == node.address);
        const indexE = this.data.links.findIndex(n => n.target.id == node.address);
        if (indexV >= 0) {
            this.data.nodes.splice(indexV, 1);
        }
        if (indexE >= 0) {
            this.data.links.splice(indexE, 1);
        }

    }

    render() {
        this.graph.graphData(this.data);
    }
}
