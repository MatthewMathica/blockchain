import Graph from './graph';
import Dag from './dag';

import GraphHandler from './handler';

export function initGraph(element, events) {

    const graph = new Graph(element);
    graph.init();

    const handler = new GraphHandler(events, graph);

    handler.handle("node", (graph, data) => {
        data.friends.forEach(function (node) {
            graph.addNode(node);
        });
        graph.render();
    });

    handler.handle("peer", (graph, node) => {
        if (node.online) {
            graph.addNode(node);
        } else {
            graph.removeNode(node);
        }
        graph.render();
    });

    handler.handle("transaction", (graph, transaction) => {
        graph.emitParticles(graph.data.links);
    });

    return handler;
}
