import Graph from './graph';
import ForceGraph from 'force-graph';

export default class Dag extends Graph {

    constructor(element) {
        super(element);
    }

    init() {

        const {offsetHeight} = this.element.parentNode;

        this.graph = ForceGraph()(this.element)
            .nodeId('hash')
            .dagMode('lr')
            .linkDirectionalParticles(2)
            .d3VelocityDecay(0.07)
            .dagLevelDistance(200)
            .nodeRelSize(3)
            .linkColor(() => 'green')
            .height(offsetHeight)
            .graphData(this.data)
            .onNodeClick(node => {
                // Center/zoom on node
                this.graph.centerAt(node.x, node.y, 1000);
                this.graph.zoom(2, 2000);
            });

    }

    addNode(node) {
        // Add node to dag
        this.data.nodes.push(node);
        // If not genesis
        if (node.prevHash != '') {
            this.data.links.push({
                source: node.prevHash,
                target: node.hash
            });
        }
    }
}
