const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const pages = ['topology', 'pool'];

module.exports = {
    entry: pages.reduce((config, page) => {
        config[page] = `./src/pages/${page}/${page}.js`;
        return config;
    }, {}),
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist'),
    },
    optimization: {
        splitChunks: {
            chunks: 'all'
        }
    },
    plugins: [new MiniCssExtractPlugin(),...pages.map(
        (page) =>
        new HtmlWebpackPlugin({
            inject: true,
            template: `./src/pages/${page}/${page}.html`,
            filename: `${page}.html`,
            chunks: [`${page}`]
        })
    )],
    module: {
        rules: [
            {
                test: /\.css$/i,
                use: [MiniCssExtractPlugin.loader, 'css-loader']
            }
        ]
    }
};
