import { Block } from "../node/tinycoin/block";

interface Mapper<T, K> {
    map(type: T): K
}

type BlockStream = ReadableStream<Block>

class StreamBlockMapper implements Mapper<ReadableStream, BlockStream> {

    constructor(){}

    map(type: ReadableStream<any>): BlockStream {
        return;
    }
}