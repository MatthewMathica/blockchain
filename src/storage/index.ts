import { Level } from "level";
import { Hashable } from "../crypto/interfaces";
import { MerkleTree } from "../node/merkle";
import { Block } from "../node/tinycoin/block";

function initStorage(dbLocation) {
    return new Level(dbLocation, { valueEncoding: 'json'});
}

const db = initStorage('./db');
const blocksSegment = db.sublevel('blocks');
const trees = db.sublevel('trees');

async function storeMerkleTree (mtree: MerkleTree): Promise<void> {
    const content: Hashable[] = mtree.getTree();
    trees.put(mtree.getRoot(), JSON.stringify(content));
}

async function retrieveMerkleTree(key: string): Promise<MerkleTree> {
    const mtree = await trees.get(key);
    return JSON.parse(mtree) as MerkleTree;
}

async function storeBlock(block: Block): Promise<void> {
    const key = block.getHash()
    blocksSegment.put(key, JSON.stringify(block))
}

async function retrieveBlock(key: string): Promise<Block> {
    const block = await blocksSegment.get(key);
    return JSON.parse(block) as Block;
}

async function storeBlocks(blocks: Block[]): Promise<void> {
    const batchQuery: any[] = blocks.map(block => ({
        type: 'put',
        key: block.getHash(),
        value: JSON.stringify(block)
    }))
    await blocksSegment.batch(batchQuery)
}

async function retrieveBlocks(keys: string[]): Promise<Block[]> {
    const rawBlocks = await blocksSegment.getMany(keys);
    return rawBlocks.map(block => JSON.parse(block));
}

async function getBlockStream(startKey: string, endKey: string): Promise<ReadableStream<any>> {

    // const readStream = new EntryStream(db, {
    //     gte: startKey,
    //     lte: endKey
    // });

    // return readStream;
    return null;
}

export { storeMerkleTree, retrieveMerkleTree, storeBlock, retrieveBlock, storeBlocks, retrieveBlocks }

