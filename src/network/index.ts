import Libp2p, { Libp2pOptions } from 'libp2p';
import {getConfig} from './configs';
import PeerId from 'peer-id';

const createNetwork = async (config: number, options: Record<string, any> = {}) => {

  // L2
  const params: Libp2pOptions = getConfig(config, options);

  // L1
  return await Libp2p.create(params);
};

export {createNetwork};
export {Modes, getConfig} from './configs';
