import KadDHT from 'libp2p-kad-dht';
import Gossipsub from 'libp2p-gossipsub';
import { Libp2pOptions } from 'libp2p';
const Bootstrap = require('libp2p-bootstrap');
const PubsubPeerDiscovery = require('libp2p-pubsub-peer-discovery');
const MDNS = require('libp2p-mdns');
const TCP = require('libp2p-tcp');
const WebRtcDirect = require('libp2p-webrtc-direct');
const Websockets = require('libp2p-websockets');
const Boostrap = require('libp2p-bootstrap');
const MPLEX = require('libp2p-mplex');
const SECIO = require('libp2p-secio');
import merge from 'ts-deepmerge';

enum Modes {
    Tcp = 1,
    Browser = 2
}

const Configs = {
    [Modes.Browser]: ({ port = 0, identity = null }) => {
        return {
            addresses: {
                listen: [`/ip4/0.0.0.0/tcp/${port}/http/p2p-webrtc-direct`]
            },
            modules: {
                streamMuxer: [MPLEX],
                connEncryption: [SECIO],
                transport: [WebRtcDirect],
                pubsub: Gossipsub,
                dht: KadDHT,
                peerDiscovery: [Bootstrap, PubsubPeerDiscovery]
            },
            config: {
                dht: {
                    enabled: true
                },
                peerDiscovery: {
                    autoDial: true,
                    bootstrap: {
                        interval: 1000,
                        list: ['/ip4/93.114.133.44/tcp/7777/p2p/QmczPh5a2WRQyp7UZ4td9C471tRjMbTed6aPdsVar29cyr'],
                        enabled: true
                    }
                }
            },
            ...(identity != null && { peerId: identity })
        }
    },
    [Modes.Tcp]: ({ port = 0, identity = null }) => {
        return {
            addresses: {
                listen: [`/ip4/0.0.0.0/tcp/${port}`]
            },
            modules: {
                transport: [TCP],
                streamMuxer: [MPLEX],
                connEncryption: [SECIO],
                pubsub: Gossipsub,
                dht: KadDHT,
                peerDiscovery: [MDNS, Bootstrap, PubsubPeerDiscovery]
            },
            config: {
                dht: {
                    enabled: true
                },
                pubsub: {
                    emitSelf: true
                },
                peerDiscovery: {
                    autoDial: true,
                    bootstrap: {
                        interval: 1000,
                        list: [
                            "/ip4/104.131.131.82/tcp/4001/ipfs/QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ",
                            "/dnsaddr/bootstrap.libp2p.io/ipfs/QmNnooDu7bfjPFoTZYxMNLWUQJyrVwtbZg5gBMjTezGAJN",
                            "/dnsaddr/bootstrap.libp2p.io/ipfs/QmQCU2EcMqAqQPR2i9bChDtGNJchTbq5TbXJJ16u19uLTa"
                        ],
                        enabled: true
                    }
                },
                relay: {                   // Circuit Relay options (this config is part of libp2p core configurations)
                    enabled: true,           // Allows you to dial and accept relayed connections. Does not make you a relay.
                    hop: {
                        enabled: true,         // Allows you to be a relay for other peers
                        active: true           // You will attempt to dial destination peers if you are not connected to them
                    }
                }
            },
            ...(identity != null && { peerId: identity })
        }
    }
};

// Bitwise map
const getConfig = function(n: number, options: Record<string, any> = {}): Libp2pOptions {

    let [c, power] = [n, 0];

    // Basic template for typechecking
    let config = {
        modules: {
            transport: [],
            streamMuxer: [],
            connEncryption: [],
            contentRouting: [],
            peerRouting: [],
            peerDiscovery: []
        }
    };

    while (c !== 0) {
        // Get config
        let configCreator = c & 1 ? Configs[1 << power] : (a) => { return {} };
        // Fill config with parameters
        const computedConfig = configCreator(options);
        config = merge(config, computedConfig);
        power++;
        c >>= 1;
    }

    return config;
}

export { getConfig, Modes };
