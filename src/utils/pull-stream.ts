/*
  Pull streams
*/

const values = function (array) {
  let i = 0;
  return function (abort, cb) {
    if (abort) cb(abort);
    return cb(i >= array.length ? true : null, array[i++]);
  };
};

const log = function () {
  return function (read) {
    return read(null, function next (end, data) {
      if (end == true) return;
      console.log(data);
      read(null, next);
    });
  };
};

const map = function (fn) {
  return function (read) {
    return function (abort, cb) {
      return read(null, function (end, data) {
        if (end == true) cb(end);
        else return cb(null, fn(data));
      });
    };
  };
};

const filter = function (predicate) {
  return function (read) {
    return function (abort, cb) {
      return read(null, function next (end, data) {
        if (end == true) cb(end);
        else if (!predicate(data)) {
          return cb(null, data);
        } else {
          return read(null, next);
        }
      });
    };
  };
};

const take = function (num) {
  return function (read) {
    let i = 0;
    return function (abort, cb) {
      return read(null, function (end, data) {
        if (end == true) cb(end);
        else return cb(i++ >= num ? true : null, data);
      });
    };
  };
};

export {log, values, map, take, filter};
