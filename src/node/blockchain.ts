import {IBlock} from './block';
import {BaseTransaction} from './transaction';
import {Hashable} from './../crypto/interfaces';
import { Emitter } from '../io/events';

export interface IBlockchain extends Hashable {
  events: Emitter<Record<string, any>>;
  blocks: IBlock[];
  addBlock(b: IBlock);
}
