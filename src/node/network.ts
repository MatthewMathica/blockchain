export {createNetwork, Modes} from './../network';

import Libp2p, { Pubsub } from 'libp2p';
import { Emitter } from '../io/events';

export interface FNode {
  address: string;
}

export interface INetwork {
  events: Emitter<Record<string, any>>;
  network: Libp2p;
  friends: FNode[];

  init(networkConfig: number, options?: Record<string, any>);

  getPubSub(): Pubsub;
  getPeerAddresses(): Array<string>;
  getPublicAddress(): string;
  getMultiAddress(): Array<string>;
}
