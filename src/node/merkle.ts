import {Hashable} from './../crypto/interfaces';
import {sha256} from './../crypto/utils';

export type MerkleProof = {
  tx_index: number;
  tx_layer_size?: number;
  hashes?: string[];
};

class MerkleCombinator implements Hashable {

  left: Hashable;
  right: Hashable;

  constructor(left: Hashable, right: Hashable) {
    this.left = left;
    this.right = right;
  }

  // Commutative Hash Combinator
  getHash(): string {
    return sha256(this.left.getHash() + this.right.getHash());
  }
}

class MerkleTree implements Hashable {
  // width = 2^n
  private _width: number;
  private _size: number;
  private _levels: number;
  private _txLayer: number;
  private _root: string = null;

  private _tree: Hashable[];

  constructor(public states: Hashable[]) {
    if (states.length < 1) {
      throw Error(`Array 'states' cannot be empty`);
    }
    const isNotPower2 = states.length & (states.length - 1);
    if (isNotPower2) {
      throw Error(`Length of states: ${states.length} is not 2^n `);
    }
    this._width = states.length;
    this._size = (this._width << 1) - 1;
    this._levels = Math.log2(this._width);
    this._txLayer = this._size - this._width;
    this._tree = [];

    // Pre-allocate array indexes
    for (let i = 0; i < this._txLayer; i++) {
      this._tree.push(null);
    }

    // Concatenate bottom layer
    this._tree = [...this._tree, ...states];

    // Calculate root
    this.derive();
  }


  getTree(): Hashable[] {
    return this._tree;
  }
  
  getHash(): string {
    return this.getRoot();
  }

  printTree(index: number = 0, level: number = 0) {
    if (index >= this._size) {
      return;
    }

    const line = ' '.repeat(level);
    console.log(line + '-' + this._tree[index].getHash());

    this.printTree(MerkleTree.getLeft(index), level + 1);
    this.printTree(MerkleTree.getRight(index), level + 1);
  }
  // N*log(N)
  private derive() {
    let i = this._levels;
    while(i > 0)
    {
      this.nextLayer(i);
      i--;
    }
    this._root = this._tree[0].getHash();
  }

  getRoot(): string {
    return this._root;
  }

  getNodes(){
    return this._tree;
  }

  static getParent(index) {
    return (index - 1) >> 1;
  }

  static getLeft(index): number {
    return (index << 1) + 1
  }

  static getRight(index): number {
    return (index + 1) << 1
  }

  static getLeftSibling(index): number {
    return index - 1;
  }

  static getRightSibling(index): number {
    return index + 1;
  }

  static verify(root: string, tx_hash: string, proof: MerkleProof): boolean {
    if (proof.tx_index < 0) return false;
    let currentIndex = proof.tx_index;
    let proofIndex = 0;
    let target = tx_hash;
    while (currentIndex > 0) {
      // If index is odd then sibling is right else left
      const rightOperation = currentIndex & 1;
      if (rightOperation) {
        target = target + proof.hashes[proofIndex];
      } else {
        target = proof.hashes[proofIndex] + target;
      }
      target = sha256(target);
      const nextIndex = rightOperation ? MerkleTree.getRightSibling(currentIndex) : MerkleTree.getLeftSibling(currentIndex);
      currentIndex = MerkleTree.getParent(nextIndex);
      proofIndex++;
    }

    return root == target;
  }

  // Log(n)
  getProof(tx_hash): MerkleProof {
    // Find transaction index
    const tx_layer_index = this._tree
      .slice(this._txLayer)
      .findIndex(tx => tx.getHash() == tx_hash);
    // Proof hashes
    const proof_hashes = [];

    if (tx_layer_index < 0) {
      return {
        tx_index: -1
      };
    }

    const tx_index = tx_layer_index + this._txLayer;
    let currentIndex = tx_index;
    // Get sibling then hash
    while (currentIndex > 0) {
      // If index is odd then sibling is right else left
      const proofIndex = currentIndex & 1 ? MerkleTree.getRightSibling(currentIndex) : MerkleTree.getLeftSibling(currentIndex);
      proof_hashes.push(this._tree[proofIndex].getHash());
      currentIndex = MerkleTree.getParent(currentIndex);
    }
    return {
      tx_index,
      tx_layer_size: this._width,
      hashes: proof_hashes
    };
  }

  // N/2
  private nextLayer(level: number) {
    const mostLeft = (1 << level) - 1;
    const mostRight = (2 << level) - 2;

    for (let i = mostLeft; i <= mostRight - 1; i+=2) {
      const parent: number = MerkleTree.getParent(i);
      const left = this._tree[i];
      const right = this._tree[i + 1];
      this._tree[parent] = new MerkleCombinator(left, right);
    }
  }
}

export { MerkleTree };
