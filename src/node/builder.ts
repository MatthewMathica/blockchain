import { IBlockchain } from "./blockchain";
import { Consensus } from "./consensus";
import { INetwork } from "./network";
import { INode } from "./node";
import { TXPool } from "./txpool";

export interface INodeBuilder {

  addNode(node: INode): INodeBuilder;
  addBlockchain(blockchain: IBlockchain): INodeBuilder;
  addNetwork(network: INetwork): INodeBuilder;
  addConsensus(consensus: Consensus): INodeBuilder;
  addTxPool(txpool: TXPool): INodeBuilder;
  build(config?: Record<string, any>): Promise<INode>;
}

export class NodeBuilder implements INodeBuilder {

  protected _node: INode;

  constructor(node: INode = null) {
    this._node = node;
  }

  addNode(node: INode): INodeBuilder {
    this._node = node;
    return this;
  }

  addNetwork(network: INetwork): INodeBuilder {
    this._node.network = network;
    return this;
  }

  addTxPool(txpool: TXPool): INodeBuilder {
    this._node.txpool = txpool;
    return this;
  }

  addConsensus(consensus: Consensus): INodeBuilder {
    this._node.consensus = consensus;
    return this;
  }

  addBlockchain(blockchain: IBlockchain): INodeBuilder {
    this._node.chain = blockchain;
    return this;
  }

  async build(config?: Record<string, any>): Promise<INode> {
    return Promise.resolve(this._node);
  }

}

