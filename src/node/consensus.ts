import crypto from 'crypto';
import {INode} from './node';
import {IBlock} from './block';

export interface Consensus {
  validate: (args: any, ... n:IBlock[]) => Promise<boolean>;
};
