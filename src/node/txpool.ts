import { BaseTransaction } from './transaction';
import { IBlock } from './block';

export type Selector = (txs: BaseTransaction[]) => IBlock;

export class TxRecord {
  removed: boolean;
  tx: BaseTransaction;

  constructor(tx: BaseTransaction) {
    this.tx = tx;
    this.removed = false;
  }
}

// Add Events / later for streaming
// This is just delayed garbage collector
export class TXPool {

  // Default maximum of 1024 transactions
  maxSize: number = 1 << 10;
  // Stored Transactions
  pool: TxRecord[] = new Array<TxRecord>();
  // TX selector -> pack many txs into block (i.e. by tx value, sender pub. key, etc...)
  selector: Selector;

  constructor() {
    // Pre-allocate array
    for (let i = 0; i < this.maxSize; i++) {
      this.pool.push({
        removed: true,
        tx: new BaseTransaction()
      });
    }
  }

  setMaxSize(size: number) {
    this.maxSize = size;
  }

  isEmpty(): boolean {
    return this.pool.every(r => r.removed);
  }

  // Non removed txs get passed to selector
  select(): IBlock {
    return this.selector(this.pool.filter(r => !r.removed).map(r => r.tx));
  }

  remove(txs: BaseTransaction[]) {
    const removeSet = new Set<string>(txs.map(s => s.getHash()));
    this.pool
      .filter(r => removeSet.has(r.tx.getHash()))
      .forEach(function(tx) {
        tx.removed = true;
      });
  }

  addAll(txs: BaseTransaction[]) {
    for (let i = 0; i < txs.length; i++) {
      this.add(txs[i]);
    }
  }

  // First-Fit heuristic
  add(tx: BaseTransaction) {
    const ref = this.pool.slice(0, this.maxSize).find(t => t.removed);
    // Allocation
    if (ref != null) {
      ref.removed = false;
      ref.tx = tx;
    }
    // TxPool is full
    // Drop transaction for now
  }

}
