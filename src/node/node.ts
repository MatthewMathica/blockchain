import {Consensus} from './consensus';
import {IBlockchain} from './blockchain';
import PeerId from 'peer-id';
import { INetwork } from './network';
import { BaseTransaction } from './transaction';
import { Emitter } from '../io/events';
import { TXPool } from './txpool';

interface INode {
  _whoami: string;
  chain: IBlockchain;
  txpool: TXPool;
  consensus: Consensus;
  network: INetwork;
  events: Emitter<Record<number, any>>;

  init();
  close();
  getNetwork(): INetwork;
  sendTransaction(t: BaseTransaction);
};

export {INode};

