import {Consensus} from './../consensus';

import {IBlock} from './../block';

export interface Pow extends Consensus {
  mine: (b: IBlock) => Promise<IBlock>;
}

export {randomString} from './../../crypto/utils';
