import {BaseTransaction} from './transaction';
import {Hashable} from './../crypto/interfaces';
import {MerkleTree} from './merkle';

export interface IBlock extends Hashable {
  prevHash: string;
  nonce: string;
  transactions: BaseTransaction[];
  merkleRoot: string;
  difficulty: number;

  // Returns false if transaction could not be added because block is already full
  addTransaction(t: BaseTransaction): boolean;
}
