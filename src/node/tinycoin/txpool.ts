import { IBlock } from '../block'
import { BaseTransaction } from '../transaction';

import { TXPool, TxRecord, Selector } from '../txpool'

import { Transaction } from './transaction'
import { Block } from './block'

const transform = (context: TXPool) => source => (async function *(){
  for await (const chunk of source) {
    yield context.select();
  }
})();


// This is just garbage collector
export class TransactionPool extends TXPool {

  outStream: any;
  inStream: any;

  constructor() {
    super();
    this.selector = (txs: BaseTransaction[]) => {
      const choosenTxs: BaseTransaction[] = txs.slice(0, Block.size);
      const block: Block = new Block();
      choosenTxs.forEach(tx => {
        block.addTransaction(tx);
      });

      return block;
    };
  }

  source() {
    return this.outStream;
  }

}
