import { IBlockchain } from './../blockchain';
import { Block } from './block';
import { IBlock } from './../block';
import { BaseTransaction } from './../transaction';
import { MerkleTree } from './../merkle';
import { MessageType } from './../../io/message';
import { Emitter, LocalEmitter } from './../../io/events';
import { Hashable } from '../../crypto/interfaces';

type MessageDefs = {
  [MessageType.BLOCK]: Block;
};

export class MetaBlock {

  val: Block;
  prev: MetaBlock;
  height: number = 0;

  constructor(val: Block, prev: MetaBlock = null) {
    this.val = val;
    this.prev = prev;
    if (prev != null) {
      this.height = prev.height + 1;
    }
  }
};

export class Blockchain implements IBlockchain {

  // Actual mainchain
  blocks: Block[];
  // Indexed mainchain
  // Add sorting by secondary key
  indexedBlocks: Map<string, MetaBlock> = new Map();
  // Orphaned chains
  orphanedBlocks: Record<string, MetaBlock> = {};
  // Current winning orphaned chain
  leadMeta: MetaBlock = null;
  // leadMeta.height - maxDifference >= all orphaned chains to become mainchain
  maxDifference: number;

  events: Emitter<MessageDefs>;

  getHash(): string {
    return new MerkleTree(this.blocks).getHash();
  }

  constructor(maxDifference: number) {
    this.maxDifference = maxDifference;

    // Init state
    this.blocks = [new Block()];

    // Genesis block
    const [genesis, ..._] = this.blocks;

    // Genesis as meta lead
    this.leadMeta = new MetaBlock(genesis);

    // Index genesis block
    this.indexedBlocks.set(genesis.getHash(), this.leadMeta);

    // Register events
    this.events = new LocalEmitter<MessageDefs>();
  }


  hasBlock(block: Block): boolean {
    return this.indexedBlocks.has(block.getHash());
  }

  addBlock(block: Block): void {

    const parentKey = block.prevHash;
    const blockKey = block.getHash();

    const parent = this.orphanedBlocks[parentKey] || this.indexedBlocks.get(parentKey);

    // Probably Scam but who knows
    if (parent == null) {
      return;
    }

    const head = new MetaBlock(block, parent);
    this.orphanedBlocks[blockKey] = head;

    if (this.maxDifference <= head.height) {
      // Persist to mainchain
      this.persistChain(head);
      // Reset score
      head.height = 0;
      // Remove orphaned chains
      this.orphanedBlocks = {};
    }

    // Ageing
    this.leadMeta.height--;
    this.leadMeta = head;
  }

  public getPath(head: MetaBlock): MetaBlock[] {
    const hash = head.val.getHash();
    // Head cannot be null (there is always genesis block except uncomplete blocks)
    if (this.indexedBlocks.has(hash)) {
      return [];
    }
    return [...this.getPath(head.prev), head];
  }

  // Copy resolved fork into mainchain
  persistChain(head: MetaBlock) {

    const metaBlocks = this.getPath(head);
    for (const metaBlock of metaBlocks) {
      // Store new block
      this.blocks.push(metaBlock.val);
      // Index new meta block
      this.indexedBlocks.set(metaBlock.val.getHash(), metaBlock);
    }
  }

};
