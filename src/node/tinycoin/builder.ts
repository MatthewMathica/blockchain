import { NodeBuilder, INodeBuilder } from "../builder";
import { INode } from "../node";
import { LightNode } from "./tinycoin";
import { Network, Modes } from './network';
import PeerId from "peer-id";
import { DogeLikeWork } from "./consensus";
import { Blockchain } from "./blockchain";
import { TransactionPool } from './txpool';

export class TinyCoinBuilder extends NodeBuilder {

  constructor() {
    super();
  }

  async build(config?: Record<string, any>): Promise<INode> {

    // Assemble components
    this.addNode(config.nodeType)
      .addConsensus(new DogeLikeWork())
      .addNetwork(new Network())
      .addTxPool(new TransactionPool())
      .addBlockchain(new Blockchain(4))

    // Initialize components
    await this._node.network.init(config.network, config);

    // Package
    return Promise.resolve(this._node);
  }
}
