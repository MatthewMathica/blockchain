import { INode } from '../node';
import { DogeLikeWork } from './consensus';
import { Block } from './block';
import { Blockchain } from './blockchain';
import { Events } from './events';
import { MessageDefs, MessageType } from './message';
import { Network, Modes } from './network';
import { Transaction } from './transaction';
import PeerId from 'peer-id';
import { Emitter, LocalEmitter } from '../../io/events';
import { TXPool } from '../txpool';

/*
  FullNode
  - p2p networking
  - validate blocks
  - mine blocks
  - send transactions
*/
export class FullNode implements INode {
    readonly _whoami: string = 'full:[TinyCoin]';

    consensus: DogeLikeWork;
    chain: Blockchain;
    network: Network;

    txpool: TXPool;
    events: Emitter<MessageDefs>;
    networkEvents: Events;

    constructor() {
        this.events = new LocalEmitter<MessageDefs>();
    }

    async init() {
        const difficulty = 4;

        this.networkEvents = new Events(this.network.getPubSub());
        // Listen blocks
        await this.networkEvents.on(MessageType.BLOCK, (block: Block) => {
            // Validate blocks
            this.consensus.validate(difficulty, block)
                .then(isValid => isValid ? this.chain.addBlock(block) : null)
                .catch(err => {
                    console.log(err);
                });
        });

        await this.networkEvents.on(MessageType.TRANSACTION, (t: Transaction) => {
            this.addTransaction(t);
        });

    };

    addTransaction(t: Transaction) {
        this.txpool.add(t);
    }

    getNetwork(): Network {
        return this.network;
    }

    sendTransaction(t: Transaction) {
        this.networkEvents.emit(MessageType.TRANSACTION, t);
        this.addTransaction(t);
        this.events.emit(MessageType.TRANSACTION, t);
    }

    async close() { }
    sendMessage(data: any) { }
}


/*
  LightNode
  - p2p networking
  - validate blocks
  - send transactions
 */
export class LightNode implements INode {
    readonly _whoami: string = 'light:[TinyCoin]';

    consensus: DogeLikeWork;
    chain: Blockchain;
    network: Network;

    txpool: TXPool;
    events: Emitter<MessageDefs>;
    networkEvents: Events;

    constructor() {
        this.events = new LocalEmitter<MessageDefs>();
    }

    async init() {
        const difficulty = 7;

        this.networkEvents = new Events(this.network.getPubSub());
        // Listen blocks
        await this.networkEvents.on(MessageType.BLOCK, (block: Block) => {
            console.log('Validating block');
            console.log(block);
            // Validate blocks
            this.consensus.validate(block.difficulty, block)
                .then(isValid => isValid ? this.chain.addBlock(block) : null);
        });

        await this.networkEvents.on(MessageType.TRANSACTION, (t: Transaction) => {
            console.log('Message:');
            console.log(t);
        });

    };

    async close() { }

    getNetwork(): Network {
        return this.network;
    }

    sendMessage(data: any) {

    }

    sendTransaction(t: Transaction) {
        this.networkEvents.emit(MessageType.TRANSACTION, t);
        this.events.emit(MessageType.TRANSACTION, t);
    }
}
