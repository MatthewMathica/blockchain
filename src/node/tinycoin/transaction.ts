import { BaseTransaction } from './../transaction';
import { sha256 } from './../../crypto/utils';
import { MessageType } from './../../io/message';

export class Transaction extends BaseTransaction {
    type: MessageType.TRANSACTION;

    public source: string;
    public target: string;
    public val: number;
    public sign: string;

    constructor() {
        super();
    }


		public toString(): string {
				return `|${this.source}|${this.target}|${this.val}|`;
		}


    public getHash(): string {
        return sha256(super.getHash());
    }
}
