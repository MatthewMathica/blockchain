// Reusing for now
import {MessageType} from './../../io/message'
import {BaseTransaction} from './../transaction';
import {Transaction} from './transaction';
import {Block}  from './block';

const toJson = (data: any): any => {
  return JSON.parse(new TextDecoder().decode(data));
}

const MessageMapper = {
  [MessageType.TRANSACTION]: (data: any): BaseTransaction => {
    return Object.assign(new Transaction(), toJson(data));
  },
  [MessageType.BLOCK]: (data: any): Block => {
    const block = Object.assign(new Block(), toJson(data));
    block.transactions = block.transactions.map(x => Object.assign(new Transaction(), x));
    return block;
  },
  [MessageType.CHAT]: (data: any): any => {
    return Object.assign({}, toJson(data));
  }
}

type Deserializator<T> = {
  [K in keyof T]: T[K] extends (...data:any[]) => any ? T[K] : never
}

type MessageDefinitions<T> = {
  [K in keyof T]: T[K] extends (...data:any[]) => infer R ? R : never
}

type MessageMapper = typeof MessageMapper;
type MessageDefs = MessageDefinitions<MessageMapper>;


export {MessageDefs, MessageType, MessageMapper};
