import { Consensus } from './../consensus';
import { randomString } from './../../crypto/utils';
import { IBlock } from './../block';

import { TXPool } from './../txpool';

import EventEmitter from 'events';

import { LocalEmitter, Emitter } from '../../io/events'
import { MessageDefs, MessageType } from './message'

// Doge is shit
export class DogeLikeWork implements Consensus {

  events: Emitter<MessageDefs>;
  txpool: TXPool;
  constructor() {
    this.events = new LocalEmitter<MessageDefs>();
  }

  setPool(txpool: TXPool): void {
    this.txpool = txpool;
  }

  // TODO: async generator
  async mineBlocks(difficulty: number) {

    // TODO: TxPool backpressured buffer - via async.queue
    while (!this.txpool.isEmpty()) {
      // Assemble block from txs
      const selectedBlock: IBlock = this.txpool.select();
      // Mine block
      const minedBlock: IBlock = await this.mine(difficulty, selectedBlock);
      // Remove transactions from pool
      this.txpool.remove(minedBlock.transactions);
      // Emit mined block
      this.events.emit(MessageType.BLOCK, minedBlock);
    }

    this.events.emit(MessageType.BLOCK, null);
  }

  async validate(difficulty: number, b: IBlock): Promise<boolean> {
    const prefix = "0".repeat(difficulty);
    return b.getHash().startsWith(prefix) ? true : false;
  }

  async mine(difficulty: number, b: IBlock): Promise<IBlock> {
    return new Promise(async resolve => {
      do {
        b.nonce = randomString(32);
      } while (await this.validate(difficulty, b) == false);

      resolve(b);
    });
  }
}
