import { IBlock } from './../block';
import { BaseTransaction } from './../transaction';
import { sha256 } from './../../crypto/utils';
import { MerkleTree } from './../merkle';

export class Block implements IBlock {
    static size = 4;

    public prevHash: string = '';
    public nonce: string = '';
    public transactions: BaseTransaction[] = [];
    public merkleRoot: string = null;
    public difficulty: number = -1;

    constructor() { }

    public addTransaction(t: BaseTransaction): boolean {
        if (this.merkleRoot != null) {
          return false;
        }

        this.transactions.push(t);
        if (Block.size == this.transactions.length) {
            // Make merkle
            this.merkleRoot = new MerkleTree(this.transactions).getRoot();
        }
        return true;
    }

		public toString(): string {
				const txs =  this.transactions.map(tx => tx.toString()).join('');
				return `${this.merkleRoot} = ${txs}`;
		}

    public getHash(): string {
        const serialized = [
            this.prevHash,
            this.nonce,
            this.transactions.map(s => s.getHash()).join(':'),
            this.merkleRoot
        ].join();

        return sha256(serialized);
    }
}
export const Genesis = () => new Block();
