import {PubsubEmitter, EventKey, EventReceiver} from '../../io/events';
import {MessageDefs, MessageMapper} from './message';

type Constructor<T> = new () => T;

export class Events extends PubsubEmitter<MessageDefs> {

  async on<K extends EventKey<MessageDefs>>(eventName: K, fn: EventReceiver<MessageDefs[K]>) {
    const fw = (t) => {
      const data = MessageMapper[eventName](t.data) as MessageDefs[K];
      fn(data);
    };
    await super.on(eventName, fw);
  };

  off<K extends EventKey<MessageDefs>>(eventName: K, fn: EventReceiver<MessageDefs[K]>) {
    super.off(eventName, fn);
  };

  emit<K extends EventKey<MessageDefs>>(eventName: K, params: MessageDefs[K]) {
    super.emit(eventName, params);
  };
}
