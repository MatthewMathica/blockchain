import {createNetwork, Modes} from './../../network/index';

import Libp2p, { Pubsub } from 'libp2p';
import PeerId from 'peer-id';
import { INetwork, FNode } from '../network';
import { Emitter, LocalEmitter } from '../../io/events';

type Events = {
  peer: any
};

export class Network implements INetwork {

  events: Emitter<Events>;
  network: Libp2p;
  friends: FNode[] = [];

  constructor() {
    this.events = new LocalEmitter<Events>();
  }

  async init(networkConfig: number, options: Record<string, any> = {}) {
    // Create network recipe
    const network = await createNetwork(networkConfig, options);

    network.on('peer:discovery', (peerId) => {
      // No need to dial, autoDial is on
      console.log('Discovered:', peerId.toB58String())
    });

    network.connectionManager.on('peer:connect', (connection) => {
      const friend = {
        address: connection.remotePeer.toB58String()
      };
      if (!this.friends.find(s => s.address == friend.address)) {
        this.friends.push(friend);
        this.events.emit("peer", {
          online: true,
          ...friend
        });
      }
    });

    network.connectionManager.on('peer:disconnect', (connection) => {
      const friend = {
        address: connection.remotePeer.toB58String()
      };
      this.friends = this.friends.filter(s => s.address != friend.address);
      this.events.emit("peer", {
        online: false,
        ...friend
      });
    });

    // Bake network from recipe
    await network.start();

    this.network = network;
  }

  public getPubSub(): Pubsub {
    return this.network.pubsub;
  }

  public getPeerAddresses(): Array<string> {
    return this.friends.map(s => s.address);
  }

  public getPublicAddress(): string {
    return this.network.peerId.toB58String();
  }

  public getMultiAddress(): Array<string> {
    return this.network.multiaddrs.map(addr => `${addr.toString()}/p2p/${this.getPublicAddress()}`);
  }
}

export {Modes};
