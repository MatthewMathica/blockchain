import {Hashable} from './../crypto/interfaces';
import {Transaction, MessageType} from './../io/message';

interface T extends Transaction, Hashable {}

// TODO: Make builder
class BaseTransaction implements T {

  type: MessageType.TRANSACTION;

  public source: string;
  public target: string;
  public val: number;
  public sign: string;

  constructor(){}

  public getHash(): string {
    return [this.source, this.target, this.val, this.sign].join(':');
  }

}

export {BaseTransaction};
