import {EventEmitter} from 'events';
import PubsubBaseProtocol from 'libp2p-interfaces/src/pubsub';

type EventMap<T> = {
  [K in keyof T]: T[K];
};

type EventKey<T extends EventMap<T>> = keyof T;
type EventReceiver<T> = (params: T) => void;

interface Emitter<T extends EventMap<T>> {
  on<K extends EventKey<T>>
    (eventName: K, fn: EventReceiver<T[K]>): void;
  off<K extends EventKey<T>>
    (eventName: K, fn: EventReceiver<T[K]>): void;
  emit<K extends EventKey<T>>
    (eventName: K, params: T[K]): void;
}

class LocalEmitter<T extends EventMap<T>> implements Emitter<T> {
  private emitter = new EventEmitter();
  on<K extends EventKey<T>>(eventName: K, fn: EventReceiver<T[K]>) {
    this.emitter.on(eventName as string, fn);
  }

  off<K extends EventKey<T>>(eventName: K, fn: EventReceiver<T[K]>) {
    this.emitter.off(eventName as string, fn);
  }

  emit<K extends EventKey<T>>(eventName: K, params: T[K]) {
    this.emitter.emit(eventName as string, params);
  }
}

class PubsubEmitter<T extends EventMap<T>> implements Emitter<T> {
  private emitter: PubsubBaseProtocol;

  constructor(emitter: PubsubBaseProtocol) {
    this.emitter = emitter;
  }

  async on<K extends EventKey<T>>(eventName: K, fn: EventReceiver<T[K]>) {
    this.emitter.on(String(eventName), fn);
    await this.emitter.subscribe(String(eventName));
  }

  off<K extends EventKey<T>>(eventName: K, fn: EventReceiver<T[K]>) {
    this.emitter.unsubscribe(String(eventName));
  }

  emit<K extends EventKey<T>>(eventName: K, params: T[K]) {
    this.emitter.publish(String(eventName), new TextEncoder().encode(JSON.stringify(params)));
  }
}



export {LocalEmitter, PubsubEmitter, Emitter, EventKey, EventReceiver};
