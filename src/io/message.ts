enum MessageType {
  TRANSACTION,
  BLOCK,
  OPERATION,
  CHAT
}

type MessageHeader = {
  source: string;
  target: string;
  type: number;
}

type Transaction = MessageHeader & {
  type: MessageType.TRANSACTION;
  val: number;
  sign: string;
}

type Block = MessageHeader & {
  type: MessageType.BLOCK;
  prevHash: string;
  nonce: string;
  transactions: Array<any>;
  merkleRoot: string;
}

type Operation = MessageHeader & {
  type: MessageType.OPERATION;
  method: (...args: any[]) => any;
  args: any[];
}

type Chat = MessageHeader & {
  type: MessageType.CHAT;
  val: string;
}

type Message = Transaction | Block | Operation | Chat;

type MessageDefs = {
  [key in MessageType]?: Extract<Message, {type: key}>
}

export {Message, MessageType, Transaction, Block, Operation, Chat, MessageDefs};
