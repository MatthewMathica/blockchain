import * as crypto from 'crypto';
import {Buffer} from 'buffer';

// Node updated remove format
const generateKeys = () => {

    const { privateKey, publicKey } = crypto.generateKeyPairSync('ec', {
        namedCurve: 'sect239k1',
        publicKeyEncoding:  { type: 'spki', format: 'pem' },
        privateKeyEncoding: { type: 'pkcs8', format: 'pem' },
        // TODO: User filled
        //passphrase: "asdasd"
    });

    return {
        pk: privateKey,
        pb: publicKey
    };
};

// TODO: Use crypto digest hex
// Use serialization inside Network
const messageToBase64 = (object: {[key: string] : string}): string => Object.values(object).map(v => Buffer.from(v, "hex")).join(":");

const signMessage = (transaction, privateKey) => {

    const dotNot = messageToBase64(transaction);
    const sign = signRawData(dotNot, privateKey);

    transaction.sign = sign;
    return transaction;
};

const verifyMessage = (transaction) => {

    const signature = transaction.sign;
    delete transaction.sign;

    const dotNot = messageToBase64(transaction);
    transaction.sign = signature;

    return verifyData(dotNot, signature, transaction.from);
};

const signRawData = (data, privateKey) => {

    const sign = crypto.createSign('SHA256');
    sign.write(data);
    sign.end();

    return sign.sign(privateKey, 'hex');
};

const verifyData = (data, signature, publicKey) => {
    const verify = crypto.createVerify('SHA256');
    verify.write(data);
    verify.end();

    return verify.verify(publicKey, signature, 'hex');
};

const randomString = (numOfBytes: number) => {
  return crypto.randomBytes(numOfBytes).toString('hex');
}

const sha256 = (data: string) => {
  return crypto.createHash('sha256').update(data).digest('hex');
};

export {
  randomString,
  sha256,
  generateKeys,
  signMessage,
  verifyMessage
};
