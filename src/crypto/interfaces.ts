interface Hashable {
  getHash(): string;
}

export {Hashable};

